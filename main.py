from math import sqrt, isqrt

def is_perfect_square(i: int) -> bool:
    return i == isqrt(i) ** 2

def get_num_of_divisors(n):
    num_of_divisors = 1
    for i in range(2, int(sqrt(n))):
        if n % i == 0:
            num_of_divisors += 1
    num_of_divisors *= 2
    num_of_divisors += 1 if is_perfect_square(n) else 0
    return num_of_divisors

found = False
acc = 0
counter = 1
while(not found):
    acc += counter
    if(get_num_of_divisors(acc) > 500): found = True
    counter += 1
print(acc)